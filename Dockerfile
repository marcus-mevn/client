FROM node:12 as base

WORKDIR /app

FROM base as node_modules

COPY package*.json ./

RUN npm ci
#RUN npm audit

FROM node_modules as build

COPY ./ .

RUN npm run lint
RUN npm run test:unit
RUN npm run build

FROM fholzer/nginx-brotli:latest AS nginx

RUN mkdir /app

COPY --from=build /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
