module.exports = {
    preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1',
        '^\\$/(.*)$': '<rootDir>/tests/unit/$1'
    },
    collectCoverage: true,
    collectCoverageFrom: [
        "<rootDir>/src/components/**/*.ts",
        "<rootDir>/src/models/**/*.ts",
        "<rootDir>/src/modules/**/*.ts",
        "<rootDir>/src/services/**/*.ts",
        "!**/node_modules/**",
        "!**/vendor/**"
    ],
    coverageThreshold: {
        global: {
            branches: 60,
            functions: 80,
            lines: 80,
            statements: 80
        }
    },
};
