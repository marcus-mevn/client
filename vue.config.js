/* eslint-disable */

const packageJson = require("./package");
const CompressionPlugin = require('compression-webpack-plugin');
// const markdown = require("markdown-js");
// const fs = require("fs");
const webpack = require("webpack");

process.env.VUE_APP_VERSION = packageJson.version;
// process.env.VUE_APP_CHANGELOG = markdown.makeHtml(fs.readFileSync("CHANGELOG.md", "utf8"));

module.exports = {
    devServer: {
        proxy: 'http://localhost:8081'
    },
    pwa: {
        name: "aaa",
        workboxPluginMode: "InjectManifest",
        workboxOptions: {
            swSrc: "./src/sw.js",
            swDest: "service-worker.js"
        },
    },
    configureWebpack: (config) => {
        config.plugins.push(...[
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        ]);

        if (process.env.NODE_ENV === "production") {
            config.plugins.push(...
                [
                    new CompressionPlugin({
                        filename: '[path].gz[query]',
                        algorithm: 'gzip',
                        test: /\.js$|\.css$|\.html$/,
                        threshold: 10240,
                        minRatio: 0.8,
                    }),
                    new CompressionPlugin({
                        filename: '[path].br[query]',
                        algorithm: 'brotliCompress',
                        test: /\.(js|css|html|svg)$/,
                        compressionOptions: { level: 11 },
                        threshold: 10240,
                        minRatio: 0.8,
                    }),
                ]
            );
        }
    }
};
